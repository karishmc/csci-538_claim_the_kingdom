# CSCI-538_Claim_the_Kingdom

Claim the Kingdom is a VR based survival game which is powered by Unity Game Engine and HTC Vive. The game starts with you being trapped in a room and tests your dodging skills! Avoiding fast speed spells with your body movements, this VR game doubles as a wonderful exercise! Survive and win the game by catching five blue colored spells:-).You lose the game if you are not able to catch these blue spells in span of your four lives. You lose a life if you are hit by spells other than blue:-(.  
